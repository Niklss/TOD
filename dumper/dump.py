from models.book import Book
import json


def dumps(book: Book) -> str:
    output = dict()
    output["name"] = book.title
    output["sentences"] = list()
    for sentence in book.iterate_sentences():
        output["sentences"].append(sentence.dumps())

    return json.dumps(output)
