from models.book import Book
from models.sentence import Sentence
from models.action import Action
import json
from StanfordDependencies.CoNLL import Token


def loads(book_data: str) -> Book:
    book_data = json.loads(book_data)
    book = Book(book_data["name"])
    for sentence_data in book_data["sentences"]:
        tokens = list()
        for token_data in sentence_data["tokens"]:
            tokens.append(Token(**token_data))
        sentence = Sentence(sentence_data["sentence_text"], tokens=tokens)

        sentence.set_action(Action(**sentence_data["action"]))
        book.add_sentence(sentence)
    return book
