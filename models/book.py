from typing import List, Dict, Any, Set, Iterable
from models.character import Character
from models.sentence import Sentence


class Book(object):
    title: str = None
    sentences: List[Sentence] = list()
    characters: Set[Character] = set()

    def __init__(self, title: str):
        self.title = title

    @classmethod
    def add_sentence(cls, sentence: Sentence):
        cls.sentences.append(sentence)

    @classmethod
    def add_sentences(cls, sentences: List[Sentence]):
        cls.sentences.extend(sentences)

    @classmethod
    def add_characters(cls, characters: List[Character]):
        cls.characters.union(characters)

    @classmethod
    def iterate_sentences(cls) -> Iterable[Sentence]:
        for sent in cls.sentences:
            yield sent

    @classmethod
    def get_characters_list(cls) -> List[Character]:
        return list(cls.characters)

    @classmethod
    def to_str(cls) -> str:
        return " ".join([x.to_str() for x in cls.sentences])
