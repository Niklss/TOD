from collections import defaultdict
from typing import List, Dict, Any, Set, Iterable, Type
from models.action import Action
from StanfordDependencies.CoNLL import Token


class Sentence(object):
    sentence_text: str = None
    type: str = None
    tokens: List[Token] = None
    action: Action = None

    def __init__(self, sentence_text: str, type: str, tokens: List[Token], action: Action = None):
        self.sentence_text = sentence_text
        self.type = type
        self.tokens = tokens
        if action:
            self.action = action

    def set_action(self, action: Action):
        self.action = action

    def iterate_tokens(self) -> Iterable[Token]:
        for token in self.tokens:
            yield token

    def to_str(self) -> str:
        return ' '.join([token.form for token in self.tokens])

    def dumps(self) -> dict:
        output = dict()
        output["sentence_text"] = self.sentence_text
        output["tokens"] = list()
        for token in self.tokens:
            output["tokens"].append({"index": token.index,
                                     "form": token.form,
                                     "cpos": token.cpos,
                                     "pos": token.pos,
                                     "head": token.head,
                                     "deprel": token.deprel,
                                     "lemma": token.lemma,
                                     "feats": token.feats,
                                     "phead": token.phead,
                                     "pdeprel": token.pdeprel,
                                     "extra": token.extra
                                     })
        output["action"] = self.action.dumps()
        return output
