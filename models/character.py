from typing import List, Dict, Any, Set


class Character(object):
    name: str = None
    age: int = -1
    voice: int = -1
    gender: int = -1

    def __init__(self, name, age, voice, gender=None):
        self.name = name
        self.age = age
        self.voice = voice
        self.gender = gender

    def __str__(self) -> str:
        return f"Character(name: {self.name}, age: {self.age}, voice: {self.voice}, gender: {self.gender})"
