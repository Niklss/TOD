from collections import defaultdict
from typing import List, Dict, Any, Set, Iterable, Type

ACTIONS_VAR = 0


class Action(object):
    key: int = None
    sentence: str
    noun: str = None
    verb: str = None
    extra: Any = None
    related_action: "Action" = None

    def __init__(self, sentence: str, key=None, noun=None, verb=None, extra=None, related_action=None):
        global ACTIONS_VAR
        ACTIONS_VAR += 1
        self.sentence = sentence
        self.noun = noun
        self.verb = verb
        self.extra = extra
        self.related_action = related_action
        if key:
            self.key = key
        else:
            self.key = ACTIONS_VAR

    def set_noun(self, noun: str):
        self.noun = noun

    def set_verb(self, verb: str):
        self.verb = verb

    def set_extra(self, extra: Any):
        self.extra = extra

    def set_related_action(self, related_action: "Action"):
        self.related_action = related_action

    def dumps(self) -> dict:
        output = dict()
        output["key"] = self.key
        output["sentence"] = self.sentence
        output["noun"] = self.noun
        output["verb"] = self.verb
        output["extra"] = self.extra
        if self.related_action:
            output["related_action"] = self.related_action.dumps()
        else:
            output["related_action"] = None
        return output
