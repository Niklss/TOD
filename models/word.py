from typing import List, Dict, Any


class Word(object):
    content: str = None
    hash: str = None

    def __init__(self, word):
        self.content = word
        # self.hash = my_hash_func(word)

    def __str__(self) -> str:
        return self.content
