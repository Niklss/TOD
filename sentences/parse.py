import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.parse.bllip import RerankingParser
from nltk.data import find
from models.book import Book
from models.book import Character
from models.sentence import Sentence
from models.action import Action
from dumper import dumps
from char_rec import entity_recognition

nltk.download('punkt')


def build_tree(sentence, parser):
    tokenized_sentence = word_tokenize(sentence)

    best = parser.parse(tokenized_sentence)
    tokens = best.get_parser_best().ptb_parse.sd_tokens()
    return tokens


def create_action_from_sentence(sentence: Sentence):
    # Fake action creation
    noun = verb = None

    nouns = [i for i in sentence.iterate_tokens() if i.pos == "NN"]
    verbs = [i for i in sentence.iterate_tokens() if i.pos == "VBP"]

    action = Action(sentence.sentence_text)
    action.set_noun(noun)
    action.set_verb(verb)
    action.set_extra({"qwerty": 123})
    action.set_related_action(Action("123"))

    return action


def define_all_roots(root, sentence):
    pos = 'VB'
    roots = list([root])
    cur = roots.copy()
    while len(cur) > 0:
        main_root = cur.pop()
        cur_roots = filter(lambda x: x.head == main_root.index and pos in x.pos, sentence.iterate_tokens())
        cur_roots = list(cur_roots)
        cur.extend(cur_roots)
        roots.extend(cur_roots)
    return roots


def main_actions(sentence: Sentence):
    pos = 'NN'
    roots = filter(lambda x: x.head == 0, sentence.iterate_tokens())
    root = list(roots)[0]
    roots = define_all_roots(root, sentence)
    root_idxs = [r.index for r in roots]

    idea = list(filter(lambda x: (x.head in root_idxs and (pos in x.pos or x.deprel == 'punct')) or x.index in root_idxs, sentence.iterate_tokens()))
    text = [token.form + ' ' for token in idea]
    with open('../main_part', 'a') as f:
        f.writelines(text)
        f.write('\n')
    action = Action(''.join(text))
    action.set_noun(list(filter(lambda x: x.deprel == 'nsubj', idea)))
    action.set_verb(list(filter(lambda x: x.pos in pos, idea)))
    action.set_extra({"qwerty": 123})
    action.set_related_action(Action("123"))

    return action


def main():
    curtext = open("../Text.txt", "r", encoding='utf-8')
    text = ''.join(curtext.readlines())
    book = Book('Text.txt')
    # parser = RerankingParser.from_unified_model_dir(find('models/bllip_wsj_no_aux').path)
    parser = RerankingParser.fetch_and_load('WSJ-PTB3', verbose=True)
    sentences = sent_tokenize(text)
    for idx, sentence in enumerate(sentences):
        s_type = None
        if '"' in sentence:
            s_type = 'dialog'
        tokens = build_tree(sentence, parser)
        sentence_obj = Sentence(sentence, s_type, tokens)
        sentence_obj.set_action(main_actions(sentence_obj))
        book.add_sentence(sentence_obj)
        chars = entity_recognition(sentence)
        characters = [Character(character[0], 0, 0, 0) for character in chars]
        book.add_characters(characters)

    with open("../dumped_book.txt", "w") as file:
        file.write(dumps(book))


if __name__ == '__main__':
    main()
