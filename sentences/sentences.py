import re


class SentenceParser:
    class Sentence:
        def __init__(self, character, sentence, isDialog):
            self.character = character
            self.sentence = sentence
            self.isDialog = isDialog

        def __repr__(self):
            return self.character + '::' + self.sentence + '::' + str(self.isDialog)

    def __init__(self):
        self.sentences = list([])
        self.parsingText()

    def addSentence(self, sentence):
        if re.match('^[\s]*?[–-]', sentence):
            parts = re.split('(?<=[.!?,])[\s]*[–-]', sentence)
            arr = [self.Sentence("char", s, False) if i % 2 else self.Sentence("char", s, True) for i, s in
                   enumerate(parts)]
            self.sentences.extend(arr)
        else:
            self.sentences.append(self.Sentence("char", sentence, False))


    def parsingText(self):
        curtext = open("/home/niklss/MyProjects/MyProjectsForBooks/Text.txt", "r", encoding='utf-8')
        text = ''.join(curtext.readlines())
        pattern = re.compile('([–-]?[^\n]+?)[\n]+?')
        arr = pattern.findall(text)
        for i in arr:
            self.addSentence(i)
        curtext.close()

        # print('\n'.join([str(line) for line in self.sentences]))


if __name__ == '__main__':
    sp = SentenceParser()