import spacy
from spacy import displacy
from collections import Counter
import en_core_web_sm


def entity_recognition(sentence, entity='PERSON'):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(sentence)
    res = [(X, X.ent_type_) for X in doc if X.ent_type_ == entity]
    return res
