bllipparser==2016.9.11
certifi==2019.11.28
JPype1==0.7.2
nltk==3.4.5
PyStanfordDependencies==0.3.1
six==1.14.0
