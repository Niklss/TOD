from models.book import Book
from models.character import Character
from models.sentence import Sentence
from models.word import Word

if __name__ == '__main__':
    test_sent = "The fox's foot grazed the sleeping dog, waking it."

    book = Book("Big Uranus")
    book.add_sentence(Sentence(test_sent))
    book.add_character(Character(name="Igor", age=22, gender=1, voice=1337))

    for i in book.iterate_sentences():
        for j in i.iterate_words():
            print(j)

    print(book.to_str())
    print(book.get_characters_list())
    print(list(map(str, book.get_characters_list())))
