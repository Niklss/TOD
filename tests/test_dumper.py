import dumper

if __name__ == '__main__':
    book = dumper.loads(open("../dumped_book.txt", "rb").read())
    print(book.to_str())